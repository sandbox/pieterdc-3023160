#Google VR View for the Web
This module creates a field formatter that lets users experience a 360° image.

##Installation

###Module
Install the module like any other Drupal module. For more information, see: 
https://www.drupal.org/docs/7/extend/installing-modules#enable_your_mod

##Library
Download the Google VR library 'vrview.min.js' from 
https://storage.googleapis.com/vrview/2.0/build/vrview.min.js and put it in a 
folder called 'vrview' inside your libraries folder, such as 
"sites/all/libraries/vrview".
To check if the library was installed correctly, go to "admin/reports/status"
and look for 'Google VR View'.

##Use
To use the module create or use an existing content type with an image field,
go to the manage display section and select 'Google VR 360 degree view' as 
format for the image field. Finally choose the settings you want.
Warning: Pictures that are not 360×180 degrees panoramas may look strange.

##Credits
The initial version of this Drupal module was inspired by the image_360degree
module.
