<?php

/**
 * @file
 * Field formatter for 360 degree images, using the Google VR View library.
 */

/**
 * Implements hook_field_formatter_info().
 */
function google_vrview_field_formatter_info() {
  return array(
    'google_vrview_image_360degree' => array(
      'label' => t('Google VR 360 degree view'),
      'field types' => array('image'),
      'settings' => array(
        'image_style' => '',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function google_vrview_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_styles = image_style_options(FALSE, PASS_THROUGH);
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function google_vrview_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_styles = image_style_options(FALSE, PASS_THROUGH);

  // Unset possible 'No defined styles' option.
  unset($image_styles['']);

  // Styles could be lost because of enabled/disabled modules that defined
  // their styles in code.
  if (isset($image_styles[$settings['image_style']])) {
    $summary[] = t('Image style: @style', array('@style' => $image_styles[$settings['image_style']]));
  }
  else {
    $summary[] = t('Original image');
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function google_vrview_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  // Load the library and check whether the library is installed.
  $lib_status = libraries_load('google_vrview');
  if ($lib_status['installed'] == FALSE || $lib_status['loaded'] == FALSE) {
    // Display error message.
    drupal_set_message($lib_status['error message'] . ' Check the readme file for further instructions on how to install the library correctly.', 'warning', FALSE);
  }

  $element = array();

  foreach ($items as $delta => $item) {
    // @todo Support multiple occurences on the same page.
    // Generate unique ID for the divs.
    //$u_id = $field['field_name'] . $delta . '_' . $entity->nid;

    $element[$delta] = array(
      '#theme' => 'image_formatter',
      '#item' => $item,
      '#image_style' => $display['settings']['image_style'],
      '#prefix' => '<div id="vrview"></div>',
    );
  }

  return $element;
}
